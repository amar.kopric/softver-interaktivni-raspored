/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellofx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author amar
 */
public class ProdekanFXMLController implements Initializable {

    @FXML
    private Button ProdekanPrikaziSveButton;
    @FXML
    private Button ProdekanAzurirajButton;
    @FXML
    private ToggleGroup godina;
    @FXML
    private Button ProdekanPodaci;
    @FXML
    private Button ProdekanGrupe;
    @FXML
    private ToggleGroup smjer;
    @FXML
    private Button SljedecaSedmicaButton;
    @FXML
    private Button ProslaSedmicaButton;
    @FXML
    private Button ProdekanUrediRasporedButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void ProdekanPrikaziSveButtonClicked(MouseEvent event) {
    }

    @FXML
    private void ProdekanAzurirajButtonClicked(MouseEvent event) {
    }

    @FXML
    private void ProdekanPodaciClicked(MouseEvent event) {
    }

    @FXML
    private void ProdekanGrupe(MouseEvent event) throws IOException {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Dodaj grupe");
        Parent root = FXMLLoader.load(getClass().getResource("ProdekanGrupeFXML.fxml"));
        window.setScene(new Scene(root));
        window.centerOnScreen();
        window.showAndWait();
    }

    @FXML
    private void SljedecaSedmicaButtonClicked(ContextMenuEvent event) {
    }

    @FXML
    private void ProslaSedmicaButtonClicked(ContextMenuEvent event) {
    }

    @FXML
    private void ProdekanUrediRasporedButtonClicked(ContextMenuEvent event) {
    }
    
}
