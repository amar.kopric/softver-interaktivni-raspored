# Softver - Interaktivni raspored u toku..

<h2>Opis sistema</h2>
Interaktivni raspored sati treba da pojednostavi proces održavanja i korištenja
rasporeda nastave na Fakultetu elektrotehnike. U sistemu je potrebno omogućiti
evidentiranje podataka o svim predmetima važećeg studijskog programa, usmjerenjima
na kojima se izvodi nastava, semestrima, nastavnicima i saradnicima koji izvode nastavu,
grupama studenata za pojedine oblike nastave za svaki predmet, zgradama i salama u
kojima se izvodi nastava i svim časovima koji se nalaze u rasporedu.
Interaktivni raspored sati namijenjen je prije svega studentima Fakulteta
elektrotehnike za efikasan pregled časova prema raznim kriterijima. Za kreiranje samog
rasporeda sati i unos opštih podataka koji su naprijed navedeni zadužen je prodekan za
nastavu i studentska pitanja. Osim njega, određene izmjene u rasporedu mogu praviti i
nastavnici, odnosno saradnici.<br>
<h3>Profil prodekana</h3>
Prodekan za nastavu i studentska pitanja je zadužen za unos podataka o svim
zgradama koje fakultet koristi za održavanje nastavnih jedinica, a zatim i za svaku zgradu
sala koje se nalaze u toj zgradi. Osim toga zadužen je za unos predmeta i usmjerenja, kao
i njihovo povezivanje kako bi se znalo koji predmet se izvodi kao obavezan predmet na
kojim usmjerenjima. Prodekan je također zadužen za unos pojedinih semestara i kreiranja
svih grupa studenata koje će pohađati taj semestar po pojedinim predmetima. Grupe seprave za svaki oblik nastave koji se izvodi na svakom predmetu, a to su grupe za
predavanja, laboratorijske i auditorne vježbe. Za svaku grupu se mora dodijeliti i
nastavnik, odnosno saradnik, koji izvodi nastavu tokom semestra toj grupi, a koji treba
biti prethodno evidentiran u sistemu. Nakon što su evidentirani svi naprijed navedeni
podaci, prodekan treba da unese časove u raspored.<br>
<h3>Nastavnički profil</h3>
Pored prodekana za nastavu i studentska pitanja interaktivni raspored sati koriste i
nastavnici/saradnici kao registrovani korisnici. Njima treba omogućiti rezervaciju
prostora za održavanje nadoknada sati, simpozija, seminara, odbrane diplomskih radova i
sl. Za svaku rezervaciju se mora znati sala koja je rezervisana, u kojem terminu, ko je
odgovorni nastavnik koji je obavio rezervaciju i u koju svrhu (oblik/tip rezervacije).
Profesor treba imati mogućnost pregleda svih rezervacija zbog sprečavanja kolizije, ali i
mogućnost izmjene i brisanja samo rezervacija koje je lično obavio. Profesoru je također,
na osnovu kreiranog rasporeda, potrebno omogućiti automatizovanu izradu mjesečnih
izvještaja o održanoj nastavi koja se treba održati u tom periodu (izgled dat u prilogu).
<h3>Neregistrovani korisnici</h3>
Neregistrovanim korisnicima (uglavnom studentima) omogućiti samo pristup
interaktivnom rasporedu, tj. samo pregled rasporeda. Zbog efikasnosti pregleda, trebaju
imati opcije filtriranja po zgradama, salama, profesorima koji izvode nastavu, godinama i
predmetima, grupama, kao i mogućnost kombiniranja više filtera istovremeno.
<h3>Projektni zadatak</h3>
Prema gore navedenim specifikacijama implementirati sistem koji će omogućiti
obavljanje svih navedenih zadataka odgovarajućim korisnicima. Sistem treba da zatraži
prijavu korisnika, te zavisno od tipa korisnika (prodekan, nastavnik/saradnik, student)
ponudi grafički interfejs putem kojeg će biti omogućeno obavljanje samo onih zadataka
koji su za datog korisnika definisani u specifikaciji sistema.<br>
<h3>So far..</h3>
<img src="slike/slika1.png">
<img src="slike/slika2.png">
<img src="slike/slika3.png">
<img src="slike/slika4.png">
<img src="slike/slika5.png">

