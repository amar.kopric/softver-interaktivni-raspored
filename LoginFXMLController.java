/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellofx;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author amar
 */
public class LoginFXMLController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Button prbutton;
    @FXML
    private Label promt;
    @FXML
    private Button studbutton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void signButton(ActionEvent event) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/testBaza", "root", "password");
            String sql = "Select * from stud where imeStud=? and mbrStud=?";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, username.getText());
            pst.setString(2, password.getText());
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                Parent root;
                String ime = "Zdenko", pass="1120";
                if(username.getText().equals(ime) && password.getText().equals(pass))
                    root = FXMLLoader.load(getClass().getResource("ProdekanFXML.fxml"));
                else
                    root = FXMLLoader.load(getClass().getResource("ProfesorFXML.fxml"));
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setTitle("Raspored");
                window.setScene(new Scene(root));
                window.centerOnScreen();
                window.show();
            } else {
                promt.setVisible(true);

            }
            con.close();
        } catch (Exception e) {

        }
    }

    @FXML
    private void studButton(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("Student.fxml"));
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setTitle("Raspored");
        window.setScene(new Scene(root));
        window.show();
    }
    
}
