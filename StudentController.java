/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hellofx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author amar
 */
public class StudentController implements Initializable {

    @FXML
    private Button StudentPrikaziSveButton;
    @FXML
    private Button StudentAzurirajButton;
    @FXML
    private Button SljedecaSedmicaButton;
    @FXML
    private Button ProslaSedmicaButton;
    @FXML
    private ToggleGroup godina;
    @FXML
    private ToggleGroup smjer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void StudentPrikaziSveButtonClicked(MouseEvent event) {
    }

    @FXML
    private void StudentAzurirajButtonClicked(MouseEvent event) {
    }

    @FXML
    private void SljedecaSedmicaButtonClicked(ContextMenuEvent event) {
    }

    @FXML
    private void ProslaSedmicaButtonClicked(ContextMenuEvent event) {
    }
    
}
